import React from 'react';
import PropTypes from 'prop-types';


class Body extends React.Component {


  constructor(props){
    super(props)
  }


  render(){

    return(

      <div className="body col-md-9">
        You can render any items as child of Nav element. All items will be
        rendered under a flex container.
      </div>

    )

  }


}

Body.propTypes = {

};

export default Body
