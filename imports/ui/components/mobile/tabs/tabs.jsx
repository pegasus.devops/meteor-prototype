import React from 'react';
import PropTypes from 'prop-types';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import "react-tabs/style/react-tabs.css";

import Home from './home/home';
import Proposals from './proposals/proposals';
import Votes from './votes/votes';


class ControlledTabs extends React.Component {

  constructor(props, context) {
    super(props, context);

    this.tabs = [{ id: 'homeTab', label: 'Home' }, { id: 'proposalsTab', label: 'Proposals' }, { id: 'votesTab', label: 'Votes' }];

    this.state = {
      key: 'homeTab',
    };
  }

  render() {
    return (
      <Tabs
        id="menu-tabs"
        activekey={ this.state.key }
        onSelect={
          (keyIndex) => {
            console.log(this.tabs[keyIndex].id);
            this.setState({ key: this.tabs[keyIndex].id })
          }
        }
      >
        <TabList>

          {
            this.tabs.map((tab) => {
              return(
                <Tab key={ tab.id } title={ tab.label } style={{ width: (this.props.screenWidth - 114) / 3 }}>
                  <div className="menuTabs">{ tab.label }</div>
                </Tab>
              )
            })

          }

        </TabList>

        <TabPanel>
          <Home/>
          <div>Device type: {this.props.deviceType}</div>
        </TabPanel>

        <TabPanel>
          <Proposals/>
          <div>Device type: {this.props.deviceType}</div>
        </TabPanel>

        <TabPanel>
          <Votes/>
          <div>Device type: {this.props.deviceType}</div>
        </TabPanel>



      </Tabs>
    );
  }
}

export default ControlledTabs
