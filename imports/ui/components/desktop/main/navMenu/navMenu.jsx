import React from 'react';
import { SideNav, Nav } from 'react-sidenav'
import {Icon} from "react-icons-kit";
import { dashboard } from "react-icons-kit/fa/dashboard";
import { users } from "react-icons-kit/fa/users";
import { shoppingCart } from "react-icons-kit/fa/shoppingCart";
import { cubes } from "react-icons-kit/fa/cubes";
import { circleO } from "react-icons-kit/fa/circleO";
import PropTypes from 'prop-types';

class NavMenu extends React.Component {


  constructor(props){

    super(props);

    this.state = { selectedPath: "1" };

  }


  onItemSelection = arg => {
    this.setState({ selectedPath: arg.path });
  };


  render(){

    return(

      <div className="nav-menu col-md-3">
        <SideNav
          defaultSelectedPath="1"
          theme={{
            selectionColor: "#FFF",
            hoverBgColor: "#181b20"
          }}
          onItemSelection={this.onItemSelection}
        >
          <Nav id="1">
            <div className="nav-menu-icons">
              <Icon icon={dashboard} />
            </div>
            <div className="nav-menu-labels">Dashboard</div>
          </Nav>
          <Nav id="2">
            <div className="nav-menu-icons">
              <Icon icon={users} />
            </div>
            <div className="nav-menu-labels">Users</div>
          </Nav>
          <Nav id="3">
            <div className="nav-menu-icons">
              <Icon icon={shoppingCart} />
            </div>
            <div className="nav-menu-labels">Deliveries</div>
          </Nav>
          <Nav id="4">
            <div className="nav-menu-icons">
              <Icon icon={circleO} />
            </div>
            <div className="nav-menu-labels">Orders</div>
          </Nav>
          <Nav id="5">
            <div className="nav-menu-icons">
              <Icon icon={cubes} />
            </div>
            <div className="nav-menu-labels">Transactions</div>
          </Nav>
        </SideNav>
      </div>

    )

  }


}

NavMenu.propTypes = {

};

export default NavMenu
