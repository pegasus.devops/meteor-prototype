import React from 'react';
import PropTypes from 'prop-types';

import NavMenu from './navMenu/navMenu'
import Body from './body/body'

class Main extends React.Component {


  constructor(props){
    super(props)
  }


  render(){
    console.log(this.props.deviceType)

    return(

      <main role="main" className="site-content row inline">
        <NavMenu/>
        <Body/>
      </main>

      )

  }


}

Main.propTypes = {

};

export default Main
