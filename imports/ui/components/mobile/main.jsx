import React from 'react';
import PropTypes from 'prop-types';

import ControlledTabs from './tabs/tabs'
// import TabNavigator from './tabs/navigator'

class Main extends React.Component {

  constructor(props){
    super(props)
  }

  render(){
    console.log(this.props.screenWidth);

    return(

      <header id="Header" >

        <ControlledTabs screenWidth={this.props.screenWidth}/>

      </header>

    )

  }

}

Main.propTypes = {

};

export default Main
